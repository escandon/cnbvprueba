﻿using CNBVSolucion.Models.BusinessLogic;
using CNBVSolucion.Models.Entities;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CNBVSolucion.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserBusiness UserBusiness = new UserBusiness();
        public ActionResult Index()
        {
            ViewBag.UserRole = CatBusiness.GetDropListRole(0);
            return View();
        }


        public ActionResult _ListUser(int? page, int roleId = 0)
        {
            //Filtros
            ViewBag.roleId = roleId;

            var data = UserBusiness.GetListUser(roleId);

            int pageSize = 7;
            int pageNumber = (page ?? 1);

            return PartialView(data.ToPagedList(pageNumber, pageSize));
        }

        [HttpGet]
        public ActionResult _CreateUser()
        {
            var user = new User
            {
                RegisterDate = DateTime.Now,
                UpdateDate = DateTime.Now
            };
            ViewBag.UserRole = CatBusiness.GetDropListRole();
            return PartialView(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _CreateUser(User user, List<int> UserRole)
        {
            string msg = "ok";
            try 
            {
                UserBusiness.CreateUser(user, UserRole);
            }
            catch(Exception e)
            {
                msg = e.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult _EditUser(int id)
        {
            var User = UserBusiness.GetUser(id);
            User.UpdateDate = DateTime.Now;
            ViewBag.UserRole = CatBusiness.GetDropListRole(User.UserRole.Select(s => s.RoleId).ToArray());
            return PartialView(User);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EditUser(User user, List<int> UserRole)
        {
            string msg = "ok";
            try
            {
                UserBusiness.EditUser(user, UserRole);
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult _DeleteUser(int UserId)
        {
            string msg = "ok";
            try
            {
                UserBusiness.DeleteUser(UserId);
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
    }
}