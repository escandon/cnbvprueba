﻿using CNBVSolucion.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CNBVSolucion.Models.BusinessLogic
{
    public class UserBusiness
    {
        public List<User> GetListUser(int roleId)
        {
            using (var db = new DB_CNBVEntities())
            {
                return db.User.Include("UserRole").Include("UserRole.Role").Where(x => x.UserRole.Select(s => s.RoleId).Contains(roleId) || roleId == 0).ToList();
            }
        }

        public User GetUser(int UserId)
        {
            using (var db = new DB_CNBVEntities())
            {
                return db.User.Include("UserRole").FirstOrDefault(x => x.UserId == UserId);
            }
        }
        public void CreateUser(User user, List<int> UserRole)
        {
            using (var db = new DB_CNBVEntities())
            {
                foreach(var item in UserRole)
                {
                    var userRole = new UserRole {
                        UserId = user.UserId,
                        RoleId = item
                    };
                    user.UserRole.Add(userRole);
                }
                db.User.Add(user);
                db.SaveChanges();
            }
        }

        public void EditUser(User user, List<int> UserRole)
        {
            using (var db = new DB_CNBVEntities())
            {
                var listUserRole = db.UserRole.Where(x => x.UserId == user.UserId).ToList();

                db.UserRole.RemoveRange(listUserRole);
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();

                foreach (var item in UserRole)
                {
                    var userRole = new UserRole
                    {
                        UserId = user.UserId,
                        RoleId = item
                    };

                    user.UserRole.Add(userRole);

                }
                db.SaveChanges();
            }
        }

        public void DeleteUser(int UserId)
        {
            using (var db = new DB_CNBVEntities())
            {
                var listUserRole = db.UserRole.Where(x => x.UserId == UserId).ToList();
                db.UserRole.RemoveRange(listUserRole);
                
                var user = db.User.Find(UserId);
                db.User.Remove(user);

                db.SaveChanges();
            }
        }
    }
    
}