﻿using CNBVSolucion.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CNBVSolucion.Models.BusinessLogic
{
    public static class CatBusiness
    {
        public static MultiSelectList GetDropListRole()
        {
            using (var db = new DB_CNBVEntities())
            {
                return new MultiSelectList(db.Role.ToList(), "RoleId", "Name");
            }
        }
        public static MultiSelectList GetDropListRole(int[] Selected) {
            using (var db = new DB_CNBVEntities())
            {
                return new MultiSelectList(db.Role.ToList(), "RoleId", "Name", Selected);
            }
        }
        public static SelectList GetDropListRole(int Select)
        {
            using (var db = new DB_CNBVEntities())
            {
                return new SelectList(db.Role.ToList(), "RoleId", "Name");
            }
        }
    }
}