USE [master]
GO
/****** Object:  Database [DB_CNBV]    Script Date: 18/08/2021 12:05:05 a. m. ******/
CREATE DATABASE [DB_CNBV]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DB_CNBV', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\DB_CNBV.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DB_CNBV_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\DB_CNBV_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DB_CNBV] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DB_CNBV].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DB_CNBV] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DB_CNBV] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DB_CNBV] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DB_CNBV] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DB_CNBV] SET ARITHABORT OFF 
GO
ALTER DATABASE [DB_CNBV] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DB_CNBV] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DB_CNBV] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DB_CNBV] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DB_CNBV] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DB_CNBV] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DB_CNBV] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DB_CNBV] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DB_CNBV] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DB_CNBV] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DB_CNBV] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DB_CNBV] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DB_CNBV] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DB_CNBV] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DB_CNBV] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DB_CNBV] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DB_CNBV] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DB_CNBV] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DB_CNBV] SET  MULTI_USER 
GO
ALTER DATABASE [DB_CNBV] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DB_CNBV] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DB_CNBV] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DB_CNBV] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [DB_CNBV] SET DELAYED_DURABILITY = DISABLED 
GO
USE [DB_CNBV]
GO
/****** Object:  Table [dbo].[Activity]    Script Date: 18/08/2021 12:05:05 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Activity](
	[ActivityId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[Active] [bit] NOT NULL,
	[RegisterDate] [smalldatetime] NOT NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Activity] PRIMARY KEY CLUSTERED 
(
	[ActivityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ActivityRole]    Script Date: 18/08/2021 12:05:05 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ActivityId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_ActivityRole] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Role]    Script Date: 18/08/2021 12:05:05 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role](
	[RoleId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[Active] [bit] NOT NULL,
	[RegisterDate] [smalldatetime] NOT NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 18/08/2021 12:05:05 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Pass] [varchar](8) NOT NULL,
	[Alias] [varchar](40) NOT NULL,
	[Name] [varchar](120) NOT NULL,
	[Active] [bit] NOT NULL,
	[RegisterDate] [smalldatetime] NOT NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 18/08/2021 12:05:05 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Activity] ([ActivityId], [Name], [Description], [Active], [RegisterDate], [UpdateDate]) VALUES (1, N' Actividad Administrar usuarios ', N' Actividad Administrar usuarios ', 1, CAST(N'2021-08-17 00:00:00' AS SmallDateTime), CAST(N'2021-08-17 00:00:00' AS SmallDateTime))
INSERT [dbo].[Activity] ([ActivityId], [Name], [Description], [Active], [RegisterDate], [UpdateDate]) VALUES (2, N'Actividad Consulta General de Usuarios ', N'Actividad Consulta General de Usuarios ', 1, CAST(N'2021-08-17 00:00:00' AS SmallDateTime), CAST(N'2021-08-17 00:00:00' AS SmallDateTime))
INSERT [dbo].[Activity] ([ActivityId], [Name], [Description], [Active], [RegisterDate], [UpdateDate]) VALUES (3, N'Actividad Generar reporte de usuarios. ', N'Actividad Generar reporte de usuarios. ', 1, CAST(N'2021-08-17 00:00:00' AS SmallDateTime), CAST(N'2021-08-17 00:00:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[ActivityRole] ON 

INSERT [dbo].[ActivityRole] ([Id], [ActivityId], [RoleId]) VALUES (1, 1, 1)
INSERT [dbo].[ActivityRole] ([Id], [ActivityId], [RoleId]) VALUES (2, 2, 2)
INSERT [dbo].[ActivityRole] ([Id], [ActivityId], [RoleId]) VALUES (3, 3, 3)
SET IDENTITY_INSERT [dbo].[ActivityRole] OFF
INSERT [dbo].[Role] ([RoleId], [Name], [Description], [Active], [RegisterDate], [UpdateDate]) VALUES (1, N'Administrador', N'Administrador', 1, CAST(N'2021-08-17 00:00:00' AS SmallDateTime), CAST(N'2021-08-17 00:00:00' AS SmallDateTime))
INSERT [dbo].[Role] ([RoleId], [Name], [Description], [Active], [RegisterDate], [UpdateDate]) VALUES (2, N'Consulta', N'Consulta', 1, CAST(N'2021-08-17 00:00:00' AS SmallDateTime), CAST(N'2021-08-17 00:00:00' AS SmallDateTime))
INSERT [dbo].[Role] ([RoleId], [Name], [Description], [Active], [RegisterDate], [UpdateDate]) VALUES (3, N'Impresor', N'Impresor', 1, CAST(N'2021-08-17 00:00:00' AS SmallDateTime), CAST(N'2021-08-17 00:00:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserId], [Pass], [Alias], [Name], [Active], [RegisterDate], [UpdateDate]) VALUES (3, N'Prueba3', N'Prueba3', N'PRueba3', 1, CAST(N'2021-08-17 22:21:00' AS SmallDateTime), CAST(N'2021-08-17 23:53:00' AS SmallDateTime))
INSERT [dbo].[User] ([UserId], [Pass], [Alias], [Name], [Active], [RegisterDate], [UpdateDate]) VALUES (6, N'Prueba2', N'Prueba2', N'PRueba2', 1, CAST(N'2021-08-17 23:09:00' AS SmallDateTime), CAST(N'2021-08-17 23:13:00' AS SmallDateTime))
INSERT [dbo].[User] ([UserId], [Pass], [Alias], [Name], [Active], [RegisterDate], [UpdateDate]) VALUES (7, N'Prueba', N'Prueba3', N'Prueba3', 1, CAST(N'2021-08-17 23:13:00' AS SmallDateTime), CAST(N'2021-08-17 23:53:00' AS SmallDateTime))
INSERT [dbo].[User] ([UserId], [Pass], [Alias], [Name], [Active], [RegisterDate], [UpdateDate]) VALUES (8, N'Prueba4', N'Prueba4', N'PRueba4', 1, CAST(N'2021-08-17 23:53:00' AS SmallDateTime), CAST(N'2021-08-17 23:53:00' AS SmallDateTime))
INSERT [dbo].[User] ([UserId], [Pass], [Alias], [Name], [Active], [RegisterDate], [UpdateDate]) VALUES (9, N'Prueba', N'Prueba5', N'Prueba5', 1, CAST(N'2021-08-17 23:54:00' AS SmallDateTime), CAST(N'2021-08-17 23:54:00' AS SmallDateTime))
INSERT [dbo].[User] ([UserId], [Pass], [Alias], [Name], [Active], [RegisterDate], [UpdateDate]) VALUES (10, N'PRueba6', N'Prueba6 ', N'Prueba6', 1, CAST(N'2021-08-17 23:54:00' AS SmallDateTime), CAST(N'2021-08-17 23:54:00' AS SmallDateTime))
INSERT [dbo].[User] ([UserId], [Pass], [Alias], [Name], [Active], [RegisterDate], [UpdateDate]) VALUES (11, N'Prueba8', N'Prueba8', N'Prueba8', 1, CAST(N'2021-08-17 23:54:00' AS SmallDateTime), CAST(N'2021-08-17 23:54:00' AS SmallDateTime))
INSERT [dbo].[User] ([UserId], [Pass], [Alias], [Name], [Active], [RegisterDate], [UpdateDate]) VALUES (13, N'Prueba10', N'Prueba10', N'Prueba10', 1, CAST(N'2021-08-18 00:02:00' AS SmallDateTime), CAST(N'2021-08-18 00:02:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[UserRole] ON 

INSERT [dbo].[UserRole] ([Id], [UserId], [RoleId]) VALUES (3, 6, 1)
INSERT [dbo].[UserRole] ([Id], [UserId], [RoleId]) VALUES (4, 6, 3)
INSERT [dbo].[UserRole] ([Id], [UserId], [RoleId]) VALUES (17, 3, 1)
INSERT [dbo].[UserRole] ([Id], [UserId], [RoleId]) VALUES (18, 7, 2)
INSERT [dbo].[UserRole] ([Id], [UserId], [RoleId]) VALUES (19, 8, 1)
INSERT [dbo].[UserRole] ([Id], [UserId], [RoleId]) VALUES (20, 9, 1)
INSERT [dbo].[UserRole] ([Id], [UserId], [RoleId]) VALUES (21, 9, 3)
INSERT [dbo].[UserRole] ([Id], [UserId], [RoleId]) VALUES (22, 10, 1)
INSERT [dbo].[UserRole] ([Id], [UserId], [RoleId]) VALUES (23, 10, 2)
INSERT [dbo].[UserRole] ([Id], [UserId], [RoleId]) VALUES (24, 11, 1)
INSERT [dbo].[UserRole] ([Id], [UserId], [RoleId]) VALUES (25, 11, 3)
INSERT [dbo].[UserRole] ([Id], [UserId], [RoleId]) VALUES (28, 13, 1)
INSERT [dbo].[UserRole] ([Id], [UserId], [RoleId]) VALUES (29, 13, 3)
SET IDENTITY_INSERT [dbo].[UserRole] OFF
ALTER TABLE [dbo].[ActivityRole]  WITH CHECK ADD  CONSTRAINT [FK_ActivityRole_Activity] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[Activity] ([ActivityId])
GO
ALTER TABLE [dbo].[ActivityRole] CHECK CONSTRAINT [FK_ActivityRole_Activity]
GO
ALTER TABLE [dbo].[ActivityRole]  WITH CHECK ADD  CONSTRAINT [FK_ActivityRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([RoleId])
GO
ALTER TABLE [dbo].[ActivityRole] CHECK CONSTRAINT [FK_ActivityRole_Role]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([RoleId])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Role]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_User]
GO
USE [master]
GO
ALTER DATABASE [DB_CNBV] SET  READ_WRITE 
GO
